from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from rest_framework import serializers

from app.models import Member

import hashlib
import json
import time
from slugify import slugify

IDPASSPORT_LENGTH = 50

def generate_idpassport(email):
    data = '{0}{1}'.format(email, get_random_string(IDPASSPORT_LENGTH))
    json_str = json.dumps([data])
    return hashlib.md5(json_str.encode()).hexdigest()

def generate_idname(name):
    names = name.split(' ')
    idname = '@{0}'.format(names[0])
    names = names[1:]
    for n in names:
        idname = '{0}.{1}'.format(idname, slugify(n))

    return check_generate_idname(idname)

def check_generate_idname(idname):

    def help_check_generate_idname(idname, count):
        new_idname = '{0}{1}'.format(idname, count)
        if Member.objects.filter(idname = new_idname).count() == 0:
            return new_idname
        else:
            return help_check_generate_idname(idname, count + 1)

    if Member.objects.filter(idname = idname).count() == 0:
        return idname
    else:
        return help_check_generate_idname(idname, 1)

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email')

    def create(self, validated_data):
        return User.objects.create_user(validated_data['username'], email=validated_data['email'], password=validated_data['password'])

    def update(self, instance, validated_data):
        instance.password = validated_data.get('password', instance.password)
        instance.save()
        return instance


class MembersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('id', 'idname', 'name', 'gender', 'image')


class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('id', 'idname', 'email', 'password', 'name', 'gender', 'image')

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.name = validated_data.get('gender', instance.name)
        instance.password = validated_data.get('password', instance.password)
        instance.idpass = validated_data.get('idname', instance.idname)
        instance.save()
        return instance


class SignupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = ('email', 'password', 'name', 'gender', 'image')

    def create(self, validated_data):
        validated_data['idname'] = generate_idname(validated_data['name'])
        validated_data['idpass'] = generate_idpassport(validated_data['email'])
        return Member.objects.create(**validated_data)


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('image',)

    def update(self, instance, validated_data):
        instance.image = validated_data.get('image', instance.image)
        instance.save()
        return instance
