from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout, models
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, authentication_classes, parser_classes, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from app.models import Member
from app.serializers import MembersSerializer, MemberSerializer, \
    SignupSerializer, UserSerializer, ImageSerializer

import json
import re

# Create your views here.

class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return


@csrf_exempt
def login(request):
    if request.method in ('POST'):
        data = JSONParser().parse(request)
        user = authenticate(username=data['email'], password=data['password'])
        if user is not None:
            if user.is_active:
                member = get_object_or_404(Member, email=data['email'])
                serializer = MemberSerializer(member)
                auth_login(request, user)
                token = Token.objects.get_or_create(user=user)
                return JsonResponse({'data': serializer.data, 'token': token.__str__()}, status=status.HTTP_200_OK)
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)
    return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
def logout(request):
    if request.method in ('GET', 'POST'):
        auth_logout(request)
        return HttpResponse(status=status.HTTP_200_OK)
    return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
def signup(request):
    if request.method in ('POST'):
        if request.POST.get('password') and len(request.POST['password']) < 8:
            raise ValidationError('Password is a short.')
        elif re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", request.POST['email']) is None:
            raise ValidationError('Email is a invalid.')
        user_serializer = UserSerializer(data={
            'username': request.POST['email'],
            'password': request.POST['password'],
            'email': request.POST['email']
        })
        if user_serializer.is_valid():
            signup_serializer = SignupSerializer(data={
                'password': request.POST['password'],
                'email': request.POST['email'],
                'name': request.POST['name'],
                'gender': request.POST['gender'],
                'image': request.FILES['image']
            })
            if signup_serializer.is_valid():
                signup_serializer.save()
                user_serializer.save()
                return JsonResponse(signup_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(user_serializer.errors, status=400)
    return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class Members(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = MembersSerializer

    def get(self, request, *args, **kwargs):
        member = Member.objects.filter(~Q(email = request.user.email))
        serializer = self.serializer_class(member, many=True)
        my_member = Member.objects.filter(email = request.user.email)
        my_serializer = self.serializer_class(my_member, many=True)
        return Response({
            "my": my_serializer.data,
            "members": serializer.data
        })


class MemberDetail(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = MemberSerializer

    def get(self, request, *args, **kwargs):
        member = get_object_or_404(Member, idname=kwargs["idname"])
        serializer = self.serializer_class(member)
        if member.email != request.user.email:
            serializer.data.pop("password", None)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        member = get_object_or_404(Member, idname=kwargs["idname"], email=request.user.email)

        request.data['id'] = 0
        request.data['idpass'] = ''
        request.data['email'] = ''
        request.data['image'] = ''

        serializer = self.serializer_class(instance=member, data=request.data)
        if serializer.is_valid():
            if serializer.data.get('password') and len(serializer.data['password']) < 8:
                raise ValidationError('Password is a short.')
            serializer.save()
            serializer.data.pop("id", None)
            serializer.data.pop("idpass", None)
            serializer.data.pop("email", None)
            serializer.data.pop("image", None)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=400)

    def put(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class ImageMember(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)
    serializer_class = ImageSerializer

    def post(self, request, *args, **kwargs):
        member = get_object_or_404(Member, idname=kwargs["idname"], email=request.user.email)
        serializer = self.serializer_class(instance=member, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=400)
