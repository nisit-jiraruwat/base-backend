from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^members/$', views.Members.as_view(), name='members-detail'),
    url(r'^member/(?P<idname>[a-zA-Z0-9_\-\.]+)/$', views.MemberDetail.as_view(), name='member-detail'),
    url(r'^imagemember/(?P<idname>[a-zA-Z0-9_\-\.]+)/$', views.ImageMember.as_view(), name='image-member'),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=('json',))
