My Back-end
========

This is Project basic Login and Sign Up on Django server.

Installation
------------
Clone project:
``` bash
$ git clone git@gitlab.com:nisit-jiraruwat/base-backend.git
```

Install package for Django(Python3):
``` bash
$ apt-get install python3-pip
```
``` bash
$ pip3 install -r requirements.txt
```
Migrate database(sqlite) for Django:
``` bash
$ python3 backend/manage.py migrate
```
Runserver for Django:
``` bash
$ python3 backend/manage.py runserver 81
```

Links
------------
Project necessary:
- [My Front-end](https://gitlab.com/nisit-jiraruwat/base-frontend)
